package com.example.springresilience4jratelimiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringResilience4jRateLimiterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringResilience4jRateLimiterApplication.class, args);
	}

}
