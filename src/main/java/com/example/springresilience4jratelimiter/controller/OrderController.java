package com.example.springresilience4jratelimiter.controller;

import java.time.LocalTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;

@RestController
public class OrderController {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
    private static final String ORDER_SERVICE ="orderService" ;

	@GetMapping("/order")
	@RateLimiter(name=ORDER_SERVICE, fallbackMethod = "rateLimiterFallback")
	public ResponseEntity<String> getOrder(){
		logger.info(LocalTime.now() + " Call processing finished = " + Thread.currentThread().getName());
		return new ResponseEntity<String>("Order is fetched", HttpStatus.OK);
	}
	
	public ResponseEntity<String> rateLimiterFallback(Exception ex){
		return new ResponseEntity<String>("reached the maximum limit for accessing order api", HttpStatus.TOO_MANY_REQUESTS);
	}
}
